# Benachrichtigungen feiner einstellen

**Einstellungen > Benachrichtigungen**

Hier müssen ggf. Erfahrungen gemacht werden, die bei der Einschätzung helfen, welche Benachrichtigungen man wirklich zeitnah braucht und wann ein hin-und-wieder-vorbeischauen ausreicht. Man muss seine Arbeit erledigen können und darf nicht durch Matrix gestört werden. 



Wir alle müssen in Zeiten der digitalen Ablenkung lernen, wie Benachrichtigungen schrittweise sinnvoll angepasst werden sollten. Wenn Sie einen Antrag für ein Forschungsprojekt schreiben und die Frist bald abläuft, möchten Sie vielleicht benachrichtigt werden, wenn Ihre Kollegen eine Diskussion mit Ihnen beginnen wollen. Wenn Sie sich in einem Raum befinden, dessen Hauptzweck es ist, über neue wissenschaftliche Arbeiten und andere interessante Dinge zu informieren, können Sie die Benachrichtigungen vllt. eher abschalten und sich selbst daran erinnern, den Raum von Zeit zu Zeit zu betreten.






