# Personen finden und direkte Nachrichten versenden

Um einzelne Personen anzuschreiben und somit einen 1:1 Chat zu erzeugen ist als Erstes auf das + in der Kategorie „Direkte Nachrichten“ zu klicken:

![14_Direktnachricht1.webp](./Screenshots/14_Direktnachricht1.webp)

Nun ist in das Suchfeld zu tippen und bspw. die E-Mail-Adresse der empfangenen Person anzufangen zu tippen:

![14_Direktnachricht2.webp](./Screenshots/14_Direktnachricht2.webp)

Personen, die schon einen Account in Matrix haben sind auch durch ihren Anzeigenamen (meist „Vorname Nachname“) auffindbar. Personen, die noch nicht eingeloggt waren, sind ausschließlich über ihre E-Mail-Adresse oder ihren ZIH-Login auffindbar. Der Link-Button „Show More“ lässt weitere Sucherergebnisse erscheinen. 

Wenn Sie niemanden finden können, fragen Sie nach deren Benutzernamen, teilen Sie ihren Benutzernamen (@ZIH-Login:tu-dresden.de) oder [Profil-Link](https://matrix.to/#/@_Ihr_ZIH-Login_:tu-dresden.de).

Eine Einladungs-E-Mail wird nicht durch Matrix versendet.

Beachten Sie, dass Matrix-Accounts von ZIH-Funktionslogins möglicherweise nicht geprüft werden. Aufgrund der Neuheit des Mediums für Viele sowie der fehlenden Multi-Account-Funktionalität vom Matrix-Client Riot, werden TU Dresden Mitarbeitende womöglich eher ihren persönlichen ZIH-Login nutzen.

Im Suchergebnis ist auf die Zielperson zu klicken:

![14_Direktnachricht3.webp](./Screenshots/14_Direktnachricht3.webp)

und dann auf Los:

![14_Direktnachricht4.webp](./Screenshots/14_Direktnachricht4.webp)

Es öffnet sich das Gespräch. Ein unverschlüsseltes Gespräch kann nach Annahme der Einladung durch die verbundene Person beginnen. Die Verbindung zum Server an der TU Dresden ist verschlüsselt, jedoch sind (wie bei E-Mails und den meisten anderen Chattools) alle Inhalte innerhalb des TU Dresden Servers gespeichert. Wer eine Ende-zu-Ende-Verschlüsselung zu Gesprächspartnern wünscht, findet dazu [hier](e2ee.md) weitere Informationen.

Weiteres: [Nachrichten schreiben](nachrichten-schreiben.md)


