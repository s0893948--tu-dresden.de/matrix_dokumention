# Ende-zu-Ende-Verschlüsselung nutzen

Die Entscheidung, ob ein Gespräch derartig verschlüsselt werden soll muss gut überlegt sein und kann nicht mehr rückgängig gemacht werden. Wenn es sich um größere oder öffentliche Räume handelt, könnte das Prüfen aller Schlüssel (alle Geräte aller Gesprächspartner) viel Zeit in Anspruch nehmen. Diese manuelle Prüfung kann man aber auch später bei Gelegenheit durchführen und auch direkt Ende-zu-Ende-verschlüsselte Gespräche mit vorerst blindem Vertrauen starten.



Wer in einem eine Ende-zu-Ende-Verschlüsselung (E2EE) zu Gesprächspartnern wünscht, erreicht dies durch Klick auf die Raumeinstellungen (z.B. über das Zahnrad oben):

![14_Direktnachricht5.webp](./Screenshots/14_Direktnachricht5.webp)

Dazu ist im Reiter Sicherheit & Datenschutz der Schieberegler Verschlüsselt zu bewegen:

![14_Direktnachricht6.webp](./Screenshots/14_Direktnachricht6.webp)

Dies kann mit OK bestätigt werden. Ab sofort können die Nachrichten nur noch von den am Gespräch Beteiligten gelesen werden. Sofern nicht schon vorab in den Einstellungen eine Schlüsselsicherung eingerichtet wurde, wird dies hier nochmals vorgeschlagen durchzuführen. So können auch bei Verlust des Gerätes, Schließen des Tabs o.a. Vorfälle die Schlüssel für frühere Gespräche wiederverwendet werden. Sie sind mit dem Master-Passwort auf dem Server der TU Dresden verschlüsselt gespeichert:

![14_Direktnachricht7.webp](./Screenshots/14_Direktnachricht7.webp)

Nun kann der verschlüsselte Austausch beginnen. Wenn man sich von der Korrektheit der Schlüssel überzeugen, und diese Vertrauenswürdigkeit digital dokumentieren möchte, ist dazu zuerst auf das Personensymbol oben rechts die Seitenleiste auszuklappen:

![14_Direktnachricht8.webp](./Screenshots/14_Direktnachricht8.webp)

In der sich öffnenden Leiste der am Gespräch Beteiligten kann nun auf die Kontaktperson geklickt werden:

![14_Direktnachricht9.webp](./Screenshots/14_Direktnachricht9.webp)

In der Liste der Geräte der Kontaktperson kann nun jedes Gerät verifiziert werden, d.h. ein manueller Schlüsselabgleich durchgeführt werden. 

![14_Direktnachricht10.webp](./Screenshots/14_Direktnachricht10.webp)

Diese Verifizierung sollte mit der Kontaktperson durch Abgleich (z.B. mündlichen via Telefon, im selben Zimmer o.a. Medium) geschehen. Da dies nicht immer leicht ist, kann auch ersteinmal das Vertrauen ausgesprochen werden (sonst wird man immer wieder gefragt, die Verifizierung durchzuführen) und bei Gelegenheit (bspw. beim nächsten Meeting) durchzuführen.

![14_Direktnachricht11.webp](./Screenshots/14_Direktnachricht11.webp)

Der Abgleich selbst geschieht über Emoji-Bildchen, die je nach Gerät und Iconpack verschieden aussehen können. Auch ist die Übersetzung aller Oberflächenelemente ins Deutsche nicht 100%-ig vorhanden.

![16_E2EE.webp](./Screenshots/16_E2EE.webp)

Analog geschieht dies in Räumen mit mehreren Teilnehmenden. Da dies anstrengend erscheint, warten alle auf die zeitnahe Einführung des Cross-Signings.
