# Räume erstellen und Verantwortung übernehmen

Neue Räume werden über das + in der linken Leiste in der Kategorie Räume erstellt.

![19_Raeume1.webp](./Screenshots/19_Raeume1.webp)

Anschließend ist der Raumname zu vergeben. Auch kann optional ein Thema (das später öfter angepasst werden kann) vergeben werden. Optional kann der Raum auch öffentlich zugänglich gemacht werden (dies ist nicht die Standardeinstellung). Weiterhin kann verhindert werden, dass Matrix-Nutzende von außerhalb des TU Dresden Heimatservers (Homeservers) den Raum betreten können.

![19_Raeume3.webp](./Screenshots/19_Raeume3.webp)

Durch Klick auf das Zahnrad oben rechts gelangt man in die Raumeinstellungen:

![19_Raeume4.webp](./Screenshots/19_Raeume4.webp)

Hier kann im Reiter **Allgemein** ein raumspezifisches Bild/Icon hochgeladen werden; eine lokale Raumadresse vergeben werden, die von Menschen leichter gelesen werden kann als der kryptische Raumname, den man unter dem Reiter Erweitert sehen kann. Eine weitere wichtige Einstellmöglichkeit ist hier, ob der Raum im Raum-Verzeichnis der TU Dresden auftauchen soll. Auch die URL-Vorschau für den Raum zu aktivieren kann hier eingestellt werden.

![19_Raeume5.webp](./Screenshots/19_Raeume5.webp)

Im Reiter **Sicherheit & Datenschutz** sind für Raumadministrator:innen wichtige Entscheidungen zu treffen: Soll der Raum verschlüsselt werden? Wer darf Zugang erhalten? Und wer darf den bisherigen Chatverlauf lesen?

![19_Raeume6.webp](./Screenshots/19_Raeume6.webp)

Die Ende-zu-Ende-Verschlüsselung größerer oder öffentlicher Räume ist zur Zeit noch nicht zu empfehlen.

Ein "Anklopfen" an geschlossene Räume ist bisher nicht möglich. Der nahestehendste Workaround ist, an die raumadministrierende Person eine Direkte Nachricht zu senden, die einen dann einlädt.

Als Raumadministrierende Person haben Sie die Verantwortung für die im Raum geteilten Inhalte (bspw. Falschnachrichten, Hetze etc.). Binden Sie weitere Personen in diese Verantwortung ein, in dem Sie in der rechten Leiste (nach Klick auf das Personensymbol) über das Drop-Down-Menü „Berechtigungslevel“ Rollen vergeben, bspw. zu Administrator:innen oder Moderator:innen.

![14_Direktnachricht12.webp](./Screenshots/14_Direktnachricht12.webp)

Über die Admin-Werkzeuge können Sie auch auf etwaiges Fehlverhalten reagieren.
