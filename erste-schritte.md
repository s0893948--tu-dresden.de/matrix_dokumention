# Empfehlungen zu ersten Schritten nach dem Erstlogin

1. Aktivieren der Desktop-Benachrichtigungen:

![05_Erstlogin.webp](./Screenshots/05_Erstlogin.webp)Dies kann später auch rückgängig gemacht werden und insb. können einzelne „Gespräche“ in ihrer Benachrichtigungsbefugnis eingestellt werden.

2. Einstellungen im Einstellungsmenü vornehmen: dazu ist auf die Zeile der E-Mail-Adresse und des nach unten zeigenden Dreiecks zu klicken:
   
   ![06_Einstellungen1.webp](./Screenshots/06_Einstellungen1.webp)
   
   Und anschließend auf die Zeile „Einstellungen“:
   
   ![06_Einstellungen2.webp](./Screenshots/06_Einstellungen2.webp)

3. In den Einstellungen können Sie im Reiter **Allgemein** bei Bedarf Ihren Anzeigenamen ändern („Vorname Nachname“) und ein Profilbild hochladen (analog zur Kontakbox auf der TUD-Website):
   ![07_Anzeigename.webp](./Screenshots/07_Anzeigename.webp)
   
   Mittelfristig wird der Anzeigename aus dem Common Name im LDAP der TU Dresden erhalten, dann ist ein manuelles Ändern nicht mehr nötig.
   
   Das E-Mail-Adressfeld ist nicht unbedingt zu füllen, da über Ihren ZIH-Login eine E-Mail-Adresse hinterlegt ist. Theoretisch können hier weitere Adressen hinzugefügt werden, bspw. um Benachrichtigungen über verpasste Nachrichten an eine weitere E-Mail-Adresse gesandt zu bekommen.
   
   Aud der selben Seite kann auch das Design Thema von hell zu dunkel verändert werden.

4. Im Reiter **Benachrichtigungen** können Sie E-Mail-Benachrichtigungen (um über verpasste Nachrichten informiert zu werden) sowie akustische Benachrichtigungen aktivieren und diese granular für einzelne Aktivitäten Anderer einstellen:
   ![08_Benachrichtigungen.webp](./Screenshots/08_Benachrichtigungen.webp)

5. Im Reiter Sprache & Video können Sie den Matrix-Client Riot berechtigen Ihre Medien (Kamera + Mikrofon) zu nutzen sowie bei Videotelefonaten sich selbst in einem kleinen Bild zu sehen:
   
   ![09_Medienberechtigungen.webp](./Screenshots/09_Medienberechtigungen.webp)

6. Im Reiter **Sicherheit & Datenschutz** finden Sie alle Ihre Geräte, die bisher vom Matrix-Account genutzt wurden. Diesen können Sie Öffentliche Namen geben, damit Sie (und Andere) diese nachvollziehen können. Dies ist für die Ende-zu-Ende-Verschlüsselung relevant, da bisher noch die Schlüssel aller Geräte (bspw. Laptop + Handy) von Gesprächspartnern geprüft werden können. Zeitnah wird es möglich sein, Gesprächspartnern zu vertrauen, die ihrerseits wiederum  nur noch die Schlüssel all ihrer Geräte untereinander zu prüfen haben (genannt Cross-Signing und schon jetzt als experimentelle Funktion in dem Reiter Labor zu finden). 
   ![10_Sicherheit.webp](./Screenshots/10_Sicherheit.webp)

7. Die **Schlüsselsicherung** ist eine wertvolle Errungenschaft, da Sie ermöglicht, die Schlüssel aller Gespräche, die Ende-zu-Ende-verschlüsselt sind, mit einem Passwort versehen zentral auf dem Server der TU Dresden zu sichern. Dies ermöglicht bequem die Nutzung mehrerer Geräte bzw. Matrix-Clients.

![11_BackupPassphrase.webp](./Screenshots/11_BackupPassphrase.webp)
