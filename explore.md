# Räume finden

Das Raumverzeichnis, in dem die öffentlich zugänglichen Räume präsentiert werden, erreicht man über den Butten „Entdecke / Explore“ links oben unter dem eigenen Anzeigenamen. Hier kann sofort nach einem Raum auf dem TUD-Matrix gesucht werden.

Da die globale Föderation zur Zeit noch ausgeschaltet ist (da erstmal TUD-interner Betrieb) können keine weiteren Räume aufgefunden werden.
