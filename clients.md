# Installation eines Clients / Programms

Der empfohlene Client zur Nutzung von Matrix heißt Riot und kann u.a. als Web-App, die an der TU Dresden installiert ist, direkt im Browser geöffnet werden: 
[https://matrix.tu-dresden.de](https://matrix.tu-dresden.de)

Empfehlenswerter ist jedoch die Installation des Programms Riot auf dem eigenen Rechner. Hier kann unabhängig vom Browser der Überblick behalten werden (allerdings sollte man sich auch um die Aktualisierungen des Programms kümmern). 

Riot kann auf dem Desktop-Rechner sowie auf Mobilgeräten installiert werden:

- [Riot für Windows / macOS / Linux](https://riot.im/desktop.html)
- [RiotX für Android](https://play.google.com/store/apps/details?id=im.vector.riotx?hl=de) (bzw. googlesicher mit [RiotX von F-Droid](https://f-droid.org/de/packages/im.vector.riotx/))
- [Riot für iOS](https://itunes.apple.com/us/app/vector.im/id1083446067)

![12_Riot-Download.webp](./Screenshots/12_Riot-Download.webp)

Nach einer Desktop-Installation ist darauf zu achten, den bestehenden Account mit dem ZIH-Login zu nutzen, und keinen neuen Account auf einem anderen Server zu erstellen. Hier am Beispiel von Riot:

![17_Anmeldung.webp](./Screenshots/17_Anmeldung.webp)

Dies wird durch Klick auf **Ändern** realisiert. Dann landet man nicht versehentlich auf einem falschen Server...

![17_MatrixOrg.webp](./Screenshots/17_MatrixOrg.webp)

Nun kann man manuell Angabe des Heimservers durchführen: https://matrix.tu-dresden.de

![17_Heimserver.webp](./Screenshots/17_Heimserver.webp)

Anschließend ist der einmalige Login mit ZIH-Login und ZIH-Passwort durchzuführen:

![17_ZIH-Login.webp](./Screenshots/17_ZIH-Login.webp)

Mit der Aktivierung des Schiebereglers unter Einstellungen > Einstellungen > „**Nach System-Login automatisch starten**“ startet der Riot-Client nach jedem Neustart und man verpasst keine Benachrichtigungen mehr durch ein versehentliches Schließen des Browser-Tabs in der Nutzungsvariante mit der Web-App.

![13_System-Login.webp](./Screenshots/13_System-Login.webp)

Die folgende Bilderreihe zeigt Bildschirmfotos der Einrichtung von Android RiotX:

![15_Android1.webp](./Screenshots/15_Android1.webp)

![15_Android2.webp](./Screenshots/15_Android2.webp)

![15_Android3.webp](./Screenshots/15_Android3.webp)
