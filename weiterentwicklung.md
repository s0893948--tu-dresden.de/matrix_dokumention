# Weiterentwicklung von Matrix

Zur Zeit bezahlt die TU Dresden nicht für die Nutzung des Quellcodes oder die Administration des Dienstes. 

Matrix ist ein offener Standard, die Details zur interoperablen Kommunikation unter Verwendung des Matrix-Sets der HTTP-APIs sind frei veröffentlicht. Matrix ist auch quelloffen, was bedeutet, dass der Quellcode der Referenz-Server, -Clients und -Dienste unter der Apache-Lizenz v2 für die Öffentlichkeit freigegeben wurde, um jeden und jede dazu zu ermutigen, ihre eigenen Server und Clients zu betreiben.

Da es sich um freie Software handelt, können Verbesserungswünsche in direkte Programmieraufträge umgewandelt werden.

Aktuell noch in der Entwicklung, und durch Aufträge/Spenden förderbar/beschleunigbar, sind u.a.

* Cross-Signing (Vertrauen in Personen, nicht in einzelne Geräte von Personen)

* Threading (übersichtlicher gestaltete Diskussionen)

* Anzeigename = LDAP Common Name

Diskussionen zu dem Thema können im Raum (folgt) durchgeführt werden.
