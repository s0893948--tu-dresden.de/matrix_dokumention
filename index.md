# Matrix - Chat an der TU Dresden

Matrix ist ein freies und offenes, sicheres, dezentralisiertes Protokoll für Echtzeit-Kommunikation, das auch unter dem Namen eines seiner nutzenden Programme, Riot, bekannt ist.

![00_matrix-riot-screenshot.webp](./Screenshots/00_matrix-riot-screenshot.webp)

Zur Zusammenarbeit in Teams stieg in den letzten Jahren der Bedarf an unterstützenden digitalen Werkzeugen (engl. tool). Ein zentrales Werkzeug ist dabei ein Team-Chat. Ein Chat bezeichnet, laut Wikipedia, „die elektronische Kommunikation mittels geschriebenem Text in Echtzeit, meist über das Internet“ ([Quelle]([https://de.wikipedia.org/wiki/Chat](https://de.wikipedia.org/wiki/Chat)). Die dazugehörige Handlung nennt man „chatten“. Mit einem Chattool (manchmal auch Messenger genannt) können Teams sich gegenseitig auf aktuelle Informationen aufmerksam machen und insb. Verknüpfungen (Hyperlinks / Links) zur weitergehenden Zusammenarbeit teilen (bspw. zur Terminfindung, zum kollaborativen Schreiben, zum Planen von Events, zum Bearbeiten von Daten, Code, Mindmaps oder Prozessen). Viele Teams an der TU Dresden haben sich aufgrund des bisher fehlenden zentralen Angebots eigene Lösungen gesucht, die zum Teil datenschutzbedenklich sind oder nicht mit anderen Systemen verknüpfbar ist.

Zur Deckung des Bedarfes an Echtzeitkommunikation wurde 2019, nach vergleichender Analyse mehrerer potentieller Lösungsoptionen, innerhalb der TU Dresden das offene Kommunikationsprotokoll Matrix in einem Pilotbetrieb eingeführt. 2020 wurde der Pilotbetrieb in den Regelbetrieb überführt. U.a. in der [digitalen Lehre](https://invidio.us/watch?v=AtkA-sE-9uU) fand Matrix als Ergänzung zu [OPAL](https://bildungsportal.sachsen.de/opal) Anwendung.

* [Warum Matrix und kein anderes Chat-System?](warum.md)

* [Wie kann Matrix genutzt werden? (Anmeldung)](start.md)

* [Empfehlungen zu ersten Schritten nach Erstlogin](erste-schritte.md)

* [Installation eines Clients / Programms](clients.md)

* [Personen finden, direkte Nachrichten eröffnen](direkte-nachrichten.md)

* [Nachrichten schreiben](nachrichten-schreiben.md)

* [Ende-zu-Ende-Verschlüsselung nutzen](e2ee.md)

* [Räume erstellen und Verantwortung übernehmen](raeume.md)

* [Räume teilen und publik machen](raeume-teilen.md)

* [Räume finden](explore.md)

* [Benachrichtigungen feiner einstellen](benachrichtigungen.md)

* [Integrations, Bridges, Bots nutzen](integrations.md)

* [Weitere Clients](weitere-clients.md)

* [Communities als Raum-Filter einsetzen](communities.md)

* [Weiterentwicklung von Matrix](weiterentwicklung.md)

* [Datenschutzerklärung](https://matrix.tu-dresden.de/html/Datenschutz.html)

* [Impressum](https://matrix.tu-dresden.de/html/Impressum.html)

## Fragen / Kontakt

Allgemeine Fragen richten Sie bitte an den Servicedesk der TU Dresden:

https://tu-dresden.de/zih/dienste/service-desk

Sollte der Servicedesk nicht weiterhelfen können, werden Fragen an das dezentrale Administrationsteam, welches Matrix an der TU Dresden begleitet, weitergeleitet.

In diesem Matrix-Raum der TU Dresden können Probleme und Lösungen diskutiert werden:

[https://matrix.tu-dresden.de/#/room/#matrix-support:tu-dresden.de](https://matrix.tu-dresden.de/#/room/#matrix-support:tu-dresden.de)

Man kann bei manchen Anomalien probieren den Cache (Zwischenspeicher) zu leeren und alles neu zu laden: Einstellungen > Hilfe & Über > Cache löschen und neu laden
