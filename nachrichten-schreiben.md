# Nachrichten schreiben

Nachrichten können mit der **Enter-Taste** versendet werden. Für einen Zeilenumbruch drückt man Umschalt + Enter.

**Dateien** lassen sich bis zu einer Größe von 10MB versenden. Dazu ist die Büroklammer auszuwählen. Die Seitenleiste mit dem Dokumentensymbol zeigt die Dateien innerhalb eines Raumes an.

**Hashtags** können benutzt werden um Begriffe leichter in der Suche auffindbar zu machen.

Wenn es mehr ungelesene Nachrichten in einem Raum gibt, als der Bildschirm anzeigen kann, lässt einen ein Klick auf das Symbol rechts vom zentralen Inhalt mit Dreieck nach oben und einem Punkt nach oben zur ältesten ungelesenen Nachricht springen.

Analog springt man zum neuesten Zeitstempel einer Unterhaltung durch Klick auf das Dreieck nach unten in einem Kreis am rechtsn Rand der zentralen Inhaltsseite.
