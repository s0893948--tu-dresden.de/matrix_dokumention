# Warum Matrix und kein anderes Chat-System?

Es gibt sehr viele Dienste und Programme zum instantanen elektronischen Kommunizieren. Matrix wurde ausgewählt, da es sich durch folgende Eigenschaften auszeichnet:

- Offener Standard (keine technische Sackgasse)

- Kommunikationsprotokoll für föderierte Echtzeitkommunikation (keine isolierte Lösung sondern wie E-Mail)

- Differenzierte Kommunikation möglich (1:1, multiple Teams, themenspezifisch)

- Dezentralisierte, persistente und interoperable Kommunikation (keine zentralen Kontrollinstanzen, verbindbar mit anderen Protokollen und Tools)

- Datenschutz: Ende-zu-Ende-Verschlüsselung pro „Gespräch“ einschaltbar

- Web-Anwendung + Desktop-Client + Mobile Apps vorhanden

- Anbindung an bestehende Authentifizierungssysteme (bspw. der TU Dresden)

- Bestehende erfolgreiche Sicherheitsauditierung

- Autonomie und Kontrolle: lokal installierbar (Serverstandort TU Dresden)

- Erkennen des Anwesenheitsstatus

- Aktive Weiterentwicklung

- Aufstrebende Nutzung durch weitere Wissenschaftseinrichtungen

Ausführliche Informationen: [https://matrix.org/faq/#intro](https://matrix.org/faq/#intro)

Ausführliches Fachbegriffs-Glossar: [https://matrix.org/faq/#definitions](https://matrix.org/faq/#definitions)
