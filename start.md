# Wie kann Matrix genutzt werden?

Mitgliedern und Angehörigen der TU Dresden wird unter Einhaltung der einschlägigen gesetzlichen und rechtlichen Bestimmungen zum Datenschutz und zur IT-Sicherheit
ermöglicht, mittels ihres **ZIH-Logins** mit Angehörigen dieser und
anderer Universitäten sowie weiteren Matrix-Nutzenden (bspw.
akademischen Partner:innen) per Chat sowie Audio-/Video-Telefonie zu
kommunizieren.

Starten Sie hier: [https://matrix.tu-dresden.de](https://matrix.tu-dresden.de) 

![01_Welcome.webp](./Screenshots/01_Welcome.webp)

Hierzu ist keine Registrierung nötig, der Dienst kann sofort durch Klick auf „Anmelden“ auf der Startseite [https://matrix.tu-dresden.de](https://matrix.tu-dresden.de) genutzt werden.

![02_Login.webp](./Screenshots/02_Login.webp)

Das Dropdown-Menü „Anmelden mit:“ sollte auf „Benutzername“ belassen werden. Dann sind folgende Eingaben zu tätigen:

**Benutzername: ZIH-Login**  (nur der ZIH-Login, keine E-Mail-Adresse)

**Passwort: ZIH-Passwort**

Alternativ ist der Login auch mit der 

Es gibt keine Möglichkeit der Registrierung (wie vllt. von anderen Matrix-Servern bekannt), da den Dienst ausschließlich Personen mit ZIH-Login nutzen können.

Es folgt nach dem Erstlogin auch keine E-Mail / Bestätigungsmail.

Analog zu E-Mail-Adressen ergeben sich damit Matrix-Adressen folgender Struktur:

@ZIH-Login:tu-dresden.de

Datenschutzerklärung: [https://matrix.tu-dresden.de/html/Datenschutz.html](https://matrix.tu-dresden.de/html/Datenschutz.html)

Impressum: https://matrix.tu-dresden.de/html/Impressum.html
